# hook-example

Simply edit this README from the gitlab web interface and run `git pull` on your local copy to see the post-merge hook in action.

If no change has occured the hook will not be triggered. 

```
~/hook-example  master ✔                                                                                                            
▶ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://gitlab.com/megamorf/hook-example
   2dafa75..bd360cd  master     -> origin/master
Updating 2dafa75..bd360cd
Fast-forward
 README.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
The following files have changed
README.md

~/hook-example  master ✔                                                                                                            
▶ git pull
Already up to date.
```
